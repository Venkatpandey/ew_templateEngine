<?php
/**
 * User: pandey
 * Date: 14/04/2018
 * Time: 10:20
 */

//require ewTemplateEngine
require_once("eWTemplateEngine.php");
$name = 'eWings';
$stuff = [
    [
        'Thing' => "roses",
        'Desc'  => "red"
    ],
    [
        'Thing' => "violets",
        'Desc'  => "blue"
    ],
    [
        'Thing' => "you",
        'Desc'  => "able to solve this"
    ],
    [
        'Thing' => "we",
        'Desc'  => "interested in you"
    ]
];

$Engine = new eWTemplateEngine($stuff, 'template.tmpl', $name);
$Engine->prepareTemplate();
$Engine->prepareView();
echo $Engine->render();