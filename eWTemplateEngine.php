<?php
/**
 * User: pandey
 * Date: 14/04/2018
 * Time: 09:59
 */

class eWTemplateEngine
{
    // variable initialization
    const TAGS = [
        '{{',
        '}}'
    ] ;
    private $viewPre = '<html>
                        <body>';
    private $viewPost = '</body>
                         </html>';
    private $headerName;
    private $viewBody;
    private $finalView;
    private $templateData;

    /**
     * eWTemplateEngine constructor.
     * @param $stuff
     * @param $template
     * @param $Name
     */
    public function __construct($stuff, $template, $Name)
    {
        $this->templateData = $stuff;
        $this->viewBody = file_get_contents('testData/' . $template);
        $this->headerName = $Name;
    }

    public function prepareTemplate()
    {
        $loopexist = false;
        $template = $this->viewBody;
        $loopBody = '';
        $fullBody = '<H1>';
        $tempData  = preg_split("#\n\s*\n#Uis", $template); // contains name and stuffs

        // find and replace Name fields
        preg_match_all('/{{(.*?)}}/', $tempData[0], $matches);
        if (strpos($tempData[0], $matches[0][0])) {
            $fullBody .= str_replace(
                $matches[0][0],
                $this->headerName,
                $tempData[0]
            );
            $fullBody .=  '</H1> <BR>';
        }

        // check if looping required
        foreach ($tempData as $key => $value) {
            if(strpos($value, '#each')) {
                $goodData = explode("\n", $value);
                $loopBody = trim
                (
                    str_replace
                    (self::TAGS,
                    '',
                    $goodData[1]
                    )
                );
                $loopexist = true;
                break;
            }
        }

        // handle loop
        if ($loopexist) {
            $this->handleLoop($fullBody, $loopBody);
        } else {
            $this->viewBody = $fullBody;
        }
    }

    public function prepareView()
    {
        $this->finalView = $this->viewPre;
        $this->finalView .= $this->viewBody;
        $this->finalView .= $this->viewPost;
    }

    /**
     * @return string
     */
    public function render() : string
    {
        return $this->finalView;
    }

    /**
     * @param $fullBody
     * @param $loopBody
     */
    private function handleLoop($fullBody, $loopBody)
    {
        foreach ($this->templateData as $id => $values) {
            $find = array_keys($values);
            $replace = array_values($values);
            $loopBodys = str_replace($find, $replace, $loopBody);
            $fullBody .= '<P>' . $loopBodys . '</P><BR>';
        }
        $this->viewBody = $fullBody;
    }
}